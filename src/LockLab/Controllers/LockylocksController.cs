using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using LockLab.Models;

namespace LockLab.Controllers
{
    public class LockylocksController : Controller
    {
        private ApplicationDbContext _context;

        public LockylocksController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Lockylocks
        public IActionResult Index()
        {
            return View(_context.Lockylocks.ToList());
        }

        // GET: Lockylocks/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Lockylock lockylock = _context.Lockylocks.Single(m => m.Id == id);
            if (lockylock == null)
            {
                return HttpNotFound();
            }

            return View(lockylock);
        }

        // GET: Lockylocks/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Lockylocks/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Lockylock lockylock)
        {
            if (ModelState.IsValid)
            {
                _context.Lockylocks.Add(lockylock);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lockylock);
        }

        // GET: Lockylocks/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Lockylock lockylock = _context.Lockylocks.Single(m => m.Id == id);
            if (lockylock == null)
            {
                return HttpNotFound();
            }
            return View(lockylock);
        }

        // POST: Lockylocks/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Lockylock lockylock)
        {
            if (ModelState.IsValid)
            {
                _context.Update(lockylock);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lockylock);
        }

        // GET: Lockylocks/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Lockylock lockylock = _context.Lockylocks.Single(m => m.Id == id);
            if (lockylock == null)
            {
                return HttpNotFound();
            }

            return View(lockylock);
        }

        // POST: Lockylocks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Lockylock lockylock = _context.Lockylocks.Single(m => m.Id == id);
            _context.Lockylocks.Remove(lockylock);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
