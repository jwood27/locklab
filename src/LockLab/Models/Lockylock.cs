﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LockLab.Models
{
    public class Lockylock
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public int Pins { get; set; }
        public bool SecurityPins { get; set; }
        public bool Picked { get; set; }
        public double PickedTime { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
