﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LockLab.Models
{
    public class ApplicationContextSeedData
    {
        private ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;

        public ApplicationContextSeedData(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task EnsureSeedDataAsync()
        {
            if (await _userManager.FindByEmailAsync("woodjeffrey2@gmail.com") == null)
            {
                // Add the user.
                var user = new ApplicationUser()
                {
                    UserName = "jwood27",
                    Email = "woodjeffrey2@gmail.com"                    
                };

                await _userManager.CreateAsync(user, "P@ssw0rd!");
            }

            if (!_context.Lockylocks.Any())
            {
                // Add new Data
                var masterDud = new Lockylock()
                {
                    Name = "POS",
                    Manufacturer = "Master",
                    Pins = 6,
                    SecurityPins = false,
                    Picked = true,
                    PickedTime = 2.3
                };                

                _context.Lockylocks.Add(masterDud);                

                _context.SaveChanges();
            }
        }
    }
}
